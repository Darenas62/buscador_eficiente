var mongoose = require('../db/mongoConnect');
var mongooseRedisCache = require("mongoose-redis-cache");
var Schema = mongoose.Schema;

var parserNoticiaSchema = new Schema({
	titular : String,
	bajada : String,
	cuerpo : String,
	autor : String,
	fecha_creacion : { type: Date, default: Date.now },
	fecha_publicacion: Date,
	fecha_modificacion: { type: Date, default: Date.now },
	tags : String,
	seccion : String,
	tema : String,
	subtema : String,
	visitas : { type: Number, default: 0 },
	acceso : { type: Number, default: 0 },
	idbusqueda : {type: mongoose.Schema.Types.ObjectId, ref: 'Noticia'}
});
parserNoticiaSchema.index({'cuerpo': 'text', 'bajada': 'text', 'titular': 'text'}, {default_language: "spanish", weights: {titular: 10, bajada: 7, cuerpo: 5}});
parserNoticiaSchema.set('redisCache', true);
/**
 * CREATE A MONGOOSE MODEL - mongoose.model(modelName, schema)
 * make Model available to us outside this script.
 */
module.exports = mongoose.conn[0].model('ParserNoticia', parserNoticiaSchema );
mongooseRedisCache(mongoose);