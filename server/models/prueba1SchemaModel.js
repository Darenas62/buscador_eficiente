/********************************************************************
Connect to MongoDB
Create Mongoose Schema and Model
Export Model to make available outisde this script

Ref.
http://mongoosejs.com/docs/guide.html
*********************************************************************/

/********************************************************************
Modules Required
*********************************************************************/
var mongoose = require('../db/mongoConnect');
//===============================================================================
//Create Mongoose Schema
var Schema = mongoose.Schema;


/**
 * CREATE A MONGOOSE SCHEMA 
 * Ref. http://mongoosejs.com/docs/schematypes.html
 * MongoDb automatically create a parameter _id that can be used to uniquely
 * identify each document in the DB
 */
var prueba1Schema = new Schema({
	campo : String
});

//To add additional keys later, use the Schema#add method

/**
 * CREATE A MONGOOSE MODEL - mongoose.model(modelName, schema)
 * make Model available to us outside this script.
 */
module.exports = mongoose.conn[1].model('Prueba1', prueba1Schema );

/**
 * Open Mongoose Connection to DB
 */
/*var db = mongoose.connection;
db.on('error', console.error);
*/