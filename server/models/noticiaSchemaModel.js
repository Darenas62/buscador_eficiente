/********************************************************************
Connect to MongoDB
Create Mongoose Schema and Model
Export Model to make available outisde this script

Ref.
http://mongoosejs.com/docs/guide.html
*********************************************************************/

/********************************************************************
Modules Required
*********************************************************************/
var mongoose = require('../db/mongoConnect');
//===============================================================================
//Create Mongoose Schema
var Schema = mongoose.Schema;


/**
 * CREATE A MONGOOSE SCHEMA 
 * Ref. http://mongoosejs.com/docs/schematypes.html
 * MongoDb automatically create a parameter _id that can be used to uniquely
 * identify each document in the DB
 */
var noticiaSchema = new Schema({
	titular : String,
	bajada : String,
	cuerpo : String,
	autor : String,
	fecha_creacion : { type: Date, default: Date.now },
	fecha_publicacion: Date,
	fecha_modificacion: { type: Date, default: Date.now },
	tags : String,
	seccion : String,
	tema : String,
	subtema : String,
	visitas : { type: Number, default: 0 },
	acceso : { type: Number, default: 0 }
});

//To add additional keys later, use the Schema#add method

/**
 * CREATE A MONGOOSE MODEL - mongoose.model(modelName, schema)
 * make Model available to us outside this script.
 */
//console.log("aqui");
module.exports = mongoose.conn[0].model('Noticia', noticiaSchema );
//console.log("exporte");
//console.log(mongoose.conn[0]);
/**
 * Open Mongoose Connection to DB
 */
/*var db = mongoose.connection;
db.on('error', console.error);
*/