/*================================================================
Server side Routing
Route Declarations
=================================================================*/
	/* ========================================================== 
	Internal App Modules/Packages Required
	============================================================ */
		var noticiaRoutes = require('./routes/noticia-routes.js');	//Exchange routes
		module.exports = function(app) {
		
			/*================================================================
			ROUTES
			=================================================================*/
			app.post('/api/noticias', noticiaRoutes.createNoticia); // CRUD para indexado incremental
			app.get('/api/noticias', noticiaRoutes.getNoticias);
			app.put('/api/noticias/:noticia_id', noticiaRoutes.updateNoticia);
			app.delete('/api/noticias/:noticia_id', noticiaRoutes.deleteNoticia);
			app.post('/api/buscarNoticias', noticiaRoutes.buscarNoticias); // Ruta para realizar busquedas
			app.get('/api/seleccionarNoticia/:noticia_id', noticiaRoutes.seleccionarNoticia); //Ruta para seleccionar Noticias
		};