/* ============================================================================
Ruteamiento del lado del servidor
Definición de rutas
============================================================================ */
'use strict';
//Importar conexión a mongo
var mongoose = require('../db/mongoConnect.js');
var striptags = require('striptags');
// Importar modelos para noticias y parser
var NoticiaModel = require('../models/noticiaSchemaModel');
var ParserNoticiaModel = require('../models/parserNoticiaSchemaModel');
var Prueba1Model = require('../models/prueba1SchemaModel');

module.exports = {
    /* ========================================================================
    CREATE - $http post
    ======================================================================== */
        createNoticia:function(req, res) {
        //Dado el post proveniente de angular, se tomaran los datos para crear la nueva noticia        
            var fecha = new Date;
            fecha = Date.now;
            NoticiaModel.create({
                titular : req.body.titular,
                bajada : req.body.bajada,
                cuerpo : req.body.cuerpo,
                autor : req.body.autor,
                fecha_creacion : fecha,
                fecha_publicacion : null,
                fecha_modificacion : fecha,
                tags : req.body.tags,
                seccion : req.body.seccion,
                tema : req.body.tema,
                subtema : req.body.subtema,
                visitas : 0,
                acceso : 0                 
            },
            // Error + indexación en al insertar exitosamente
            function(err, noticia) {
                if (err) {
                    res.send(err);
                }else{
                    ParserNoticiaModel.create({
                        titular: striptags(req.body.titular.replace(/\r?\n|\r|&[a-zA-Z#0-9]*;?/g,"")),
                        bajada: striptags(req.body.bajada.replace(/\r?\n|\r|&[a-zA-Z#0-9]*;?/g,"")),
                        cuerpo: striptags(req.body.bajada.replace(/\r?\n|\r|&[a-zA-Z#0-9]*;?/g,"")),
                        autor: req.body.autor,
                        fecha_creacion: fecha,
                        fecha_publicacion: null,
                        fecha_modificacion: fecha,
                        tags: req.body.tags,
                        seccion: req.body.seccion,
                        tema: req.body.tema,
                        subtema: req.body.subtema,
                        visitas: 0,
                        acceso: 0,
                        idbusqueda: noticia._id
                    },
                    function(err, parser){
                        if(err){
                            res.send(err);
                        }else{
                            console.log(parser+"\n"+noticia);
                        };
                    });
               };
            });
        },
                                                                                                                                                                                                                                                                                
    /* ========================================================================
    READ - $http get
    ======================================================================== */
        getNoticias : function(req, res) {
            // Se llama al método find del controller para obtener todas las 
            // noticias de la base de datos
            NoticiaModel.find(function(err, noticias) {
                //Manejo de errores. 
                if (err) {
                    // Retornar error
                    return res.send(err);
                } else {
                    // Retornar noticias en formato json
                    return res.json(noticias); 
                };
            });
        },
    /* =======================================================================
        Buscar - $http POST
    ======================================================================= */
        buscarNoticias : function(req, res){
        var q = req.body.text;
        var buscarxtema	= req.body.filter;
        //Tenis, , 
        //console.log(q);
        for (var i=0; i< q.length; i++){
            var caracter = q.charAt(i);
            if (caracter == '*'){
                buscarxtema = 3;
            }
        }
        if(buscarxtema==1){
            console.log("Buscar x tema = 1, buscado:" + q);
            ParserNoticiaModel.find( { tema: {$regex: q, $options: 'i'} } )
                .limit(1000)
                .exec(function (err, items) {
                    if(err){
                        console.log("error"+err);
                        return res.send(err);
                    }
		    console.log(items);
		    // Cambiar retorno, buscar noticia por id en noticias
		    // y retornar noticia original
		    // autoincrementar campo de busquedas
                    return res.json(items);
                });
            /* -----------------------------------------------------
            
            Prueba busqueda en varias bases de datos
            Se debe crear un archivo para cada modelo, esto es requisito para trabajar con la base de datos
            --------------------------------------------------------*/
         /*   Prueba1Model.find( { campo: {$regex: q, $options:'i'} })
                .limit(1000)
                .exec(function (err, items){
                    if(err){
                        console.log("error"+err);
                        return res.send(err);
                    }
                    console.log(items);
                    return res.json(items);
                });
           */ 
        }else if (buscarxtema==2) {
            
	     console.log("Buscar x subtema = 2, buscado:" + q);
         /*
             ParserNoticiaModel.find( { $subtema: q } )
		 .limit(1000)
                 .exec(function (err, items) {
                     if(err){
			console.log("error"+err);
                        return res.send(err);
                     }
                     
		     var noticias = incremental(items,
                         function(respuesta){
                           //console.log(res);
                           return res.json(respuesta);    
                         }
                     );
                 });
                 */


            ParserNoticiaModel.find({ subtema: {$regex: q, $options: 'i'} })
                 .limit(1000)
                 .exec(function (err, items) {
                     if(err){
             console.log("error"+err);
                         return res.send(err);
                     }
                     var noticias = incremental(items,
                         function(respuesta){
                           //console.log(res);
                           return res.json(respuesta);    
                         }
                     );
                     //return res.json(items);
                 });

        }else if (buscarxtema == 3){
            console.log("**** Busqueda por comodin ****");
            var arr = q.split("*", 1);
            q = arr[0];
            console.log(q);
            ParserNoticiaModel.find({ bajada: new RegExp(q+"[a-zA-Z#0-9]*",'i') })
                .limit(1000)
                .exec(function (err,items){
                    if(err){
                        console.log("error"+err);
                        return res.send(err);
                    }
                    var noticias = incremental(items, function(respuesta){
                    
                    }
                );
                return res.json(items);
            });
        
        }else{
            // Buscar y ordenar por tema
            //ParserNoticiaModel.find({ $text: { $search: q , $language:"es"} }).sort( { "tema": 1, } ).sort( { "subtema": 1, } )
            // Buscar y ordenar por relevancia
            ParserNoticiaModel.find({ $text: { $search: q , $language:"es" } }, { score : { $meta: "textScore" } })
                 .limit(1000)
                 .sort({ score: { $meta: "textScore" } })
                 .exec(function (err, items) {
                     if(err){
			 console.log("error"+err);
                         return res.send(err);
                     }
                     var noticias = incremental(items,
                         function(respuesta){
                           //console.log(res);
                           return res.json(respuesta);    
                         }
                     );
                     //return res.json(items);
                 });
};             
        },
    
    /* ========================================================================
    UPDATE - $http put
    ======================================================================== */
        updateNoticia:function(req, res) {
            var query = { "_id" : req.params.noticia_id };
            var update = {
                            titular : req.body.titular,
                            bajada : req.body.bajada,
                            cuerpo : req.body.cuerpo,
                            autor : req.body.autor,
                            fecha_modificacion : Date.now,
                            tags : req.body.tags,
                            seccion : req.body.seccion,
                            tema : req.body.tema,
                            subtema : req.body.subtema,
                            visitas : req.body.visitas,
                            acceso : req.body.acceso    
                         };
            var id = req.params.noticia_id;
            NoticiaModel.findByIdAndUpdate(id ,update, function(err, noticia) {
                if (err) {
                    res.send(err);
                }else{
                    update.titular = striptags(update.titular.replace(/\r?\n|\r|&[a-zA-Z#0-9]*;?/g,""));
                    update.bajada = striptags(update.bajada.replace(/\r?\n|\r|&[a-zA-Z#0-9]*;?/g,""));
                    update.cuerpo = striptags(update.cuerpo.replace(/\r?\n|\r|&[a-zA-Z#0-9]*;?/g,""));
                    ParserNoticiaModel.update( {idbusqueda: noticia._id}, update, function(err,respuesta){
                        if (err){
                            res.send(err);
                        };
                    });
                };
            });
        },
    
    /* ========================================================================
    DELETE - $http delete
    ======================================================================== */
        deleteNoticia : function(req, res) {
            NoticiaModel.remove({
                _id : req.params.noticia_id
            },
            function(err, noticia) {
                if (err) {
                    res.send(err);
                }else{
                    ParserNoticiaModel.remove({
                        idbusqueda: noticia._id
                    },
                    function(err, parser){
                        if(err){
                            res.send(err);
                        };
                    });
                };
            });
        },

        seleccionarNoticia : function(req,res){
            NoticiaModel.findById(req.params.noticia_id, function(err,noticia){
                if (err) {
                    res.send(err);
                }else{
                    res.jsonp(noticia);    
                };
            });
        }
};    

function incremental(items, callback){
    var ids=[];
    var original=[];
    for (var i = 0; i < items.length; i++){
        ids.push(items[i]._id);
        original.push(items[i].idbusqueda);
        //console.log(ids[i]+"\n"+"buscado: "+items[i].visitas+"\n"+"original: "+items[i].idbusqueda);
    }
    ParserNoticiaModel.update(
        { _id: { "$in":ids } },
        { $inc: { visitas: 1 } },
        {multi: true},
        function(err, res){
            if(err){
                console.log(err);
            }else{
                console.log(res);
            }
        }
    );
    NoticiaModel.update({ _id: {"$in": original} },
        {$inc: { visitas: 1 } },
        {multi: true},
        function(err, res){
            if(err){
                console.log(err);
            }else{
                console.log(res);
            }
        }
    );
    NoticiaModel.find({_id: {"$in": original}},
        function(err,res){
            if(err){
                console.log(err);
            }else{
                
                callback(res);
                //datos = res;
                //console.log(res[0]);
                //console.log(res);
            }
        }
    );
};