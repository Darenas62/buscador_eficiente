/********************************************************************
Connect to MongoDB
Create Mongoose Schema and Model
Export Model to make available outisde this script

Ref.
http://mongoosejs.com/docs/guide.html
*********************************************************************/

/********************************************************************
Modules Required
*********************************************************************/
var mongoose = require('mongoose');
//var mongooseRedisCache = require("mongoose-redis-cache");
var database = require('../config/db'); 	//database config - i.e. Local/remote MongoDB URL
mongoose.conn = [];
//var dbUrl = database.url;
/*for (var item in database){
  console.log(database[item]);
}*/
//console.log(dbUrl);
console.log('Running mongoose version %s', mongoose.version);

/********************************************************************
Run MongoDB in safe mode - wait for INSERT operations to succeed
important when altering passwords.
*********************************************************************/
var mongoOptions = { db: { safe: true }};

/********************************************************************
Connect to MongoDB
*********************************************************************/
/*var conn = mongoose.createConnection(dbUrl, mongoOptions, function (err, res) {
  if (err) { 
    console.log ('ERROR connecting to: ' + dbUrl + '. ' + err);
  } else {
    console.log ('Successfully connected to: ' + dbUrl);
  }
});*/
var i = 0;
for (var item in database){
 //console.log("item: "+item);
 //console.log(database[item]);
 mongoose.conn[i] =  mongoose.createConnection(database[item], mongoOptions, function (err, res){
    if (err){
      console.log('ERROR: '+ err);
    } else {
      console.log('Coneccion exitosa');
    }
  });
  i++;
}
//console.log(connection[0]);
//console.log(connection[1]);


//===============================================================================
/*
 * Open Mongoose Connection to DB
 */
//var db = mongoose.connection;
//db.on('error', console.error);
module.exports = mongoose;