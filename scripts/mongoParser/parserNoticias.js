// Modulos y Conexion a mongoose
var striptags = require('striptags');
require('../../server/models/mongoConnect.js');
var NoticiaModel = require('../../server/models/noticiaSchemaModel');
var ParserNoticiaModel = require('../../server/models/parserNoticiaSchemaModel');

// La variable cursor avanzara por la coleccion noticias
var cursor = NoticiaModel.find().cursor();

// Por cada dato se llama al evento data
cursor.on('data', function (doc) {
 
  var datos = doc.toJSON(); // Obtener noticia
  var parseNoticia = new ParserNoticiaModel({ // Generar modelo
    titular: striptags(datos.titular.replace(/\r?\n|\r|&[a-zA-Z#0-9]*;?/g,"")), // Parsear HTML con striptags y Marks con regex
    bajada : striptags(datos.bajada.replace(/\r?\n|\r|&[a-zA-Z#0-9]*;?/g,"")),
    cuerpo : striptags(datos.cuerpo.replace(/\r?\n|\r|&[a-zA-Z#0-9]*;?/g,"")),
    autor : datos.autor,
    fecha_creacion : datos.fecha_creacion,
    fecha_publicacion : datos.fecha_publicacion,
    fecha_modificacion : datos.fecha_modificacion,
    tags : datos.tags,
    seccion : datos.seccion,
    tema : datos.tema,
    subtema : datos.subtema,
    visitas : datos.visitasr,
    acceso : datos.acceso,
    idbusqueda : datos._id // Guardar ID noticia original
  });
  console.log(parseNoticia.toJSON());   
  parseNoticia.save(); // Guardar noticia parseada en mongo 
}).on('error', function (err) {
    // handle the error
    console.log(err);
}).on('close', function () {
   setTimeout(function(){
    console.log("Finalizado - Datos ingresados correctamente en parsernoticias");
    console.log("CTRL + C para cerrar");
   },2000);
});


