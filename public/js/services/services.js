'use strict';

/* ============================================================================
Module - for the Services
============================================================================ */
angular.module('mongoDbApp.services', []).

factory('buscarNoticiasService', function ($http, $q){
    //console.log($http);
    var buscarNoticias = function(text){
        console.log(text);
        var deferred = $q.defer();
        $http.post('api/buscarNoticias', text).
        success(function(data) {
            deferred.resolve(data);
        }).
        error(function(reason) {
            deferred.reject(reason);
        });
        return deferred.promise
    }
    return {
        buscarNoticias: buscarNoticias
    };
}).
/**
 * getNoticias - Factory Service
 */
factory('getNoticiasService', function($http, $q) {
    
    /*=========================================================================
    READ - $http get
    ======================================================================== */
    var getNoticias = function() {
        var deferred = $q.defer();
        $http.get('/api/noticias/').
        success(function(data) {
            deferred.resolve(data);
        }).
        error(function(reason) {
            deferred.reject(reason);
        });
        return deferred.promise
    }
    //Return Factory Object
    return {
        getNoticias: getNoticias
    };
}).

/**
 * Create Noticia - Factory Service
 */
factory('createNoticiaService', function($http, $q) {

    /*=========================================================================
    CREATE - $http post
    ======================================================================== */
    var createNoticia = function(noticia) {
        var deferred = $q.defer();
        $http.post('/api/noticias/', noticia).
        success(function(data) {
            deferred.resolve(data);
        }).
        error(function(reason) {
            deferred.reject(reason);
        });
        return deferred.promise
    }
    //Return Factory Object
    return {
        createNoticia: createNoticia
    } 
}).

/**
 * Update Noticia - Factory Service
 */
factory('updateNoticiaService', function($http, $q) {

    /*=========================================================================
    UPDATE - $http put
    ======================================================================== */
    var updateNoticia = function(id, updateData) {
	    
        var deferred = $q.defer();

        $http.put('/api/noticias/' + id, updateData).
        success(function(data) {
            deferred.resolve(data);
        }).
        error(function(reason) {
            deferred.reject(reason);
        });
        return deferred.promise
    }
    //Return Factory Object
    return {
        updateNoticia: updateNoticia
    } 
}).

factory('seleccionarNoticiaService', function($http, $q){
    var noticiaSeleccionada = function(){
        var deferred = $q.defer();
        $http.get('/api/seleccionarNoticia/:noticia_id').
        success(function(data) {
            deferred.resolve(data);
        }).
        error(function(reason) {
            deferred.reject(reason);
        });
        return deferred.promise
    }
    //Return Factory Object
    return {
        noticiaSeleccionada: noticiaSeleccionada
    };
}).


/**
 * Delete Noticia - Factory Service
 */
factory('deleteNoticiaService', function($http, $q) {

    /*=========================================================================
    DELETE - $http delete
    ======================================================================== */
    var deleteNoticia = function(id) {

        var deferred = $q.defer();
        $http.delete('/api/noticias/' + id).
        success(function(data) {
            deferred.resolve(data);
        }).
        error(function(reason) {
            deferred.reject(reason);
        });
        return deferred.promise
    }
    //Return Factory Object
    return {
        deleteNoticia: deleteNoticia
    }
});
