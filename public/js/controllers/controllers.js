'use strict';

/* ============================================================================
Module - for the Controllers
============================================================================ */
angular.module('mongoDbApp.controllers', [])

/**
 * Controller - MainCtrl
 */
.controller('MainCtrl', function($http, $q, getNoticiasService,
    createNoticiaService, updateNoticiaService, deleteNoticiaService, buscarNoticiasService, $log, $scope) {



    var main =this;
    main.formData = {};

  $scope.setPage = function (pageNo) {
    $scope.currentPage = pageNo;
  };

  $scope.pageChanged = function() {
    console.log('Page changed to: ' + $scope.currentPage);
  };

$scope.setItemsPerPage = function(num) {
  $scope.itemsPerPage = num;
  $scope.currentPage = 1; //reset to first paghe
};

 
    main.buscarNoticia = function(){

    var buscar = {"text": main.noticia.text, "filter": main.noticia.buscarxtema}
    console.log(buscar);
    console.log("Variable: " + main.noticia.buscarxtema)

    buscarNoticiasService.buscarNoticias(buscar)
    .then(function(answer) {

        //console.log(answer);
        main.noticias = answer;
        $scope.data = answer;
          $scope.viewby = 10;
      $scope.totalItems = $scope.data.length;
      $scope.currentPage = 1;
      $scope.itemsPerPage = 5;
      $scope.maxSize = 5;

    },
    function(error) {
        console.log("OOPS!!!! " + JSON.stringify(error));
    });

    };

    

	   /*
     * Create a New Noticia
     */
    main.createNoticia = function() {
        createNoticiaService.createNoticia(main.formData)
        .then(function(answer) {
            main.noticias = answer;
        },
        function(error) {
        	console.log("Error Creating Noticia!!!! " + JSON.stringify(error));
        });
    };

    /*
     * Update a Noticia
     */
    main.editNoticia = function(id, txt, isDone) {
    	var updateData = {"text":txt, "done": isDone};
    	updateNoticiaService.updateNoticia(id, updateData)
    	.then(function(answer) {
    		main.noticias = answer;
    	},
    	function(error) {
    		console.log("OOPS Error Updating!!!! " + JSON.stringify(error));
    	});
    };

    /**
     * Delete a Noticia
     */
    main.deleteNoticia = function(id) {
        deleteNoticiaService.deleteNoticia(id)
        .then(function(answer) {
            main.noticias = answer;
        },
        function(error) {
            console.log("OOPS Error Deleting!!!! " + JSON.stringify(error));
        });
    };


})
    .controller('viewNoticia', function($http, $q, $routeParams, $scope, getNoticiasService,
     $log) {    
    $http.get('/api/seleccionarNoticia/' + $routeParams.noticia_id)
        .then(function successCallback(response) {
            $scope.titular = response.data.titular
            $scope.fecha_creacion = response.data.fecha_creacion
            $scope.autor = response.data.autor
            $scope.bajada = response.data.bajada
            $scope.cuerpo = response.data.cuerpo
        }, function errorCallback(err) {            
        
        });    
});