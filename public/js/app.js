'use strict';

/* ============================================================================
Module - Main App Module 'angularSimplePagination',
============================================================================ */
angular.module('mongoDbApp', ['ui.bootstrap','mongoDbApp.controllers', 'mongoDbApp.services',
    'mongoDbApp.routes']);
